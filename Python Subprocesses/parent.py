import subprocess


def main():
    print("Hello this is parent.")
    # create a child process via command line
    child = subprocess.Popen(['python3', 'child.py'])
    # wait for child thread to terminate
    child.communicate()
    code = child.returncode
    print("Child exited with status=", code)
    if code == 0:
        print("Child exited successfully.")
    else:
        print("Child exited with an error.")


if __name__ == '__main__':
    main()
