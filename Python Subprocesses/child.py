import sys
import random


def main():
    print("Hello this is child.")
    # exit with random choice of status code
    # choice of 1 indicates an error by convention
    sys.exit(random.randint(0, 1))


if __name__ == '__main__':
    main()
