import win32com.client
import os
sh = win32com.client.gencache.EnsureDispatch('Shell.Application', 0)
ns = sh.NameSpace(os.path.join(os.getcwd(), 'res'))
columns = []
while True:
    colname = ns.GetDetailsOf(None, len(columns))
    if not colname:
        break
    columns.append(colname)

for item in ns.Items():
    print(item.Path)
    for colnum, colname in enumerate(columns):
        colval = ns.GetDetailsOf(item, colnum)
        if colval:
            print('\t', colname, ':', colval)
